using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using OrchestrateApi.Common;

namespace OrchestrateApi.Email
{
    public class EmailService
    {
        private readonly AppConfiguration configuration;
        private readonly ILogger logger;
        private readonly IEmailProvider emailProvider;

        public EmailService(AppConfiguration configuration, ILogger<EmailService> logger)
        {
            this.configuration = configuration;
            this.logger = logger;
            this.emailProvider = new MailjetEmailProvider(configuration);
        }

        private string ToEmailOverride => this.configuration.ToEmailOverride;

        private string FromName => this.configuration.OrganisationName;

        private string FromAddress => this.configuration.DefaultFromEmail;

        public async Task SendAsync(EmailMessage email)
        {
            if (!string.IsNullOrEmpty(ToEmailOverride))
            {
                this.logger.LogDebug($"Email to {email.ToAddress} is being overridden");
                email.ToAddress = ToEmailOverride;
            }

            if (string.IsNullOrEmpty(email.ToAddress))
            {
                this.logger.LogDebug($"Email with subject '{email.Subject}' was not sent as it has no to address");
                return;
            }

            if (string.IsNullOrEmpty(email.FromName))
            {
                email.FromName = FromName;
            }

            if (string.IsNullOrEmpty(email.FromAddress))
            {
                email.FromAddress = FromAddress;
            }

            if (string.IsNullOrEmpty(email.FromAddress))
            {
                this.logger.LogWarning($"Email to {email.ToAddress} with subject {email.Subject} was not sent as no from address has been configured");
                return;
            }

            await this.emailProvider.SendAsync(email);
        }
    }
}