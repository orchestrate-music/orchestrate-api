using System;
using System.Threading.Tasks;
using Mailjet.Client;
using Mailjet.Client.Resources;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using OrchestrateApi.Common;

namespace OrchestrateApi.Email
{
    internal class MailjetEmailProvider : IEmailProvider
    {
        private readonly IMailjetClient client;

        public MailjetEmailProvider(AppConfiguration appConfig)
        {
            this.client = new MailjetClient(appConfig.MailjetPublicKey, appConfig.MailjetSecretKey)
            {
                Version = ApiVersion.V3_1,
            };
        }

        public async Task SendAsync(EmailMessage email)
        {
            var request = BuildRequest(email);
            var response = await this.client.PostAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Failed to send email to {email.ToAddress}: {email.Subject}");
                Console.WriteLine($"Status: {response.StatusCode}. Message: {response.GetErrorMessage()}");
                Console.WriteLine($"Info: {response.GetErrorInfo()}");
                Console.WriteLine(response.GetData());
            }
        }

        private MailjetRequest BuildRequest(EmailMessage email)
        {
            var request = new MailjetRequest { Resource = Send.Resource };
            request.Property(Send.Messages, JArray.FromObject(new[]
            {
                JObject.FromObject(new
                {
                    From = JObject.FromObject(new
                    {
                        Name = email.FromName,
                        Email = email.FromAddress,
                    }),
                    To = JArray.FromObject(new[]
                    {
                        JObject.FromObject(new
                        {
                            Name = email.ToName,
                            Email = email.ToAddress,
                        }),
                    }),
                    Subject = email.Subject,
                    TextPart = email.TextContent,
                }),
            }));

            return request;
        }
    }
}