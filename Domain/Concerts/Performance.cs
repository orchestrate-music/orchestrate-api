using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OrchestrateApi.Domain.Ensembles;

namespace OrchestrateApi.Domain.Concerts
{
    public class Performance : IEntity
    {
        public Performance()
        {
            PerformanceMembers = new HashSet<PerformanceMember>();
            PerformanceScores = new HashSet<PerformanceScore>();
        }

        [Key]
        public int Id { get; set; }

        public int EnsembleId { get; set; }
        public int ConcertId { get; set; }

        public bool CountsAsSeperate { get; set; }

        [ForeignKey(nameof(ConcertId))]
        public virtual Concert Concert { get; set; }

        [ForeignKey(nameof(EnsembleId))]
        public virtual Ensemble Ensemble { get; set; }

        public virtual ICollection<PerformanceMember> PerformanceMembers { get; set; }

        public virtual ICollection<PerformanceScore> PerformanceScores { get; set; }
    }
}
