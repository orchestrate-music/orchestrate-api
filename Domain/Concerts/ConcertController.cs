using Microsoft.AspNetCore.Mvc;
using OrchestrateApi.DAL;
using OrchestrateApi.Common;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using OrchestrateApi.Security;

namespace OrchestrateApi.Domain.Concerts
{
    [Authorize(Policy = StandardAccessRequirement.PolicyName)]
    [ApiController]
    [Route("concert")]
    public class ConcertController : ControllerBase
    {
        private readonly OrchestrateContext context;
        private readonly StandardEntityQueries<Concert> queries;

        public ConcertController(OrchestrateContext context)
        {
            this.context = context;
            this.queries = new StandardEntityQueries<Concert>(this, context);
        }

        [HttpGet]
        public Task<ActionResult<IEnumerable<Concert>>> GetConcerts()
        {
            return queries.Get();
        }

        [HttpGet("{id:int}")]
        public Task<ActionResult<Concert>> GetConcert(int id)
        {
            return queries.Get(id);
        }

        [HttpPatch("{id:int}")]
        public Task<IActionResult> PutConcert(int id, JsonPatchDocument<Concert> concertPatch)
        {
            return queries.Patch(id, concertPatch);
        }

        [HttpPost]
        public Task<ActionResult<Concert>> PostConcert(Concert concert)
        {
            return queries.Post(concert, nameof(GetConcert));
        }

        [HttpDelete("{id:int}")]
        public Task DeleteConcert(int id)
        {
            return queries.Delete(id);
        }

        [HttpGet("{concertId:int}/performance")]
        public async Task<ActionResult<IEnumerable<object>>> GetPerformances(int concertId)
        {
            var concert = await context.Concert.FindAsync(concertId);
            if (concert == null)
            {
                return NotFound();
            }

            return await context.Performance
                .Where(e => e.ConcertId == concertId)
                .Include(e => e.Ensemble)
                .Select(e => new
                {
                    e.Id,
                    e.EnsembleId,
                    e.ConcertId,
                    e.CountsAsSeperate,
                    Ensemble = new
                    {
                        e.Ensemble.Id,
                        e.Ensemble.Name,
                        e.Ensemble.IsHidden,
                    },
                })
                .ToArrayAsync();
        }
    }
}
