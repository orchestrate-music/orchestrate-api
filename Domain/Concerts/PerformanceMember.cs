using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OrchestrateApi.Domain.Members;

namespace OrchestrateApi.Domain.Concerts
{
    public class PerformanceMember
    {
        [Key]
        public int Id { get; set; }

        public int MemberId { get; set; }

        public int PerformanceId { get; set; }

        [ForeignKey(nameof(MemberId))]
        public virtual Member Member { get; set; }

        [ForeignKey(nameof(PerformanceId))]
        public virtual Performance Performance { get; set; }
    }
}
