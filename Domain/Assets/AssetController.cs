using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using Microsoft.AspNetCore.JsonPatch;
using OrchestrateApi.Common;
using Microsoft.AspNetCore.Authorization;
using OrchestrateApi.Security;

namespace OrchestrateApi.Domain.Assets
{
    [Authorize(Policy = StandardAccessRequirement.PolicyName)]
    [ApiController]
    [Route("asset")]
    public class AssetController : ControllerBase
    {
        private readonly OrchestrateContext context;
        private readonly StandardEntityQueries<Asset> queries;

        public AssetController(OrchestrateContext context)
        {
            this.context = context;
            this.queries = new StandardEntityQueries<Asset>(this, context);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<object>>> GetAssets()
        {
            return await context.Asset
                .Include(e => e.AssetLoan)
                    .ThenInclude(e => e.Member)
                .Select(e => new
                {
                    e.Id,
                    e.Description,
                    e.Quantity,
                    e.SerialNo,
                    e.DateAcquired,
                    e.ValuePaid,
                    e.DateDiscarded,
                    e.ValueDiscarded,
                    e.InsuranceValue,
                    e.CurrentLocation,
                    e.Notes,
                    LoanedTo = e.AssetLoan.Where(l => l.DateReturned == null)
                        .Select(l => new
                        {
                            l.Id,
                            l.MemberId,
                            l.AssetId,
                            l.DateBorrowed,
                            l.DateReturned,
                            Member = new
                            {
                                l.Member.Id,
                                l.Member.FirstName,
                                l.Member.LastName,
                            },
                        })
                        .FirstOrDefault(),
                })
                .ToArrayAsync();
        }

        [HttpGet("{id}")]
        public Task<ActionResult<Asset>> GetAsset(int id)
        {
            return queries.Get(id);
        }

        [HttpPatch("{id}")]
        public Task<IActionResult> PutAsset(int id, JsonPatchDocument<Asset> assetPatch)
        {
            return queries.Patch(id, assetPatch);
        }

        [HttpPost]
        public Task<ActionResult<Asset>> PostAsset(Asset asset)
        {
            return queries.Post(asset, nameof(GetAsset));
        }

        [HttpDelete("{id}")]
        public Task DeleteAsset(int id)
        {
            return queries.Delete(id);
        }

        [HttpGet("{assetId:int}/loan")]
        public async Task<ActionResult<IEnumerable<object>>> GetAssetLoans(int assetId)
        {
            return await context.AssetLoan
                .Include(e => e.Member)
                .Where(e => e.AssetId == assetId)
                .Select(e => new
                {
                    e.Id,
                    e.AssetId,
                    e.MemberId,
                    e.DateBorrowed,
                    e.DateReturned,
                    Member = new
                    {
                        e.Member.Id,
                        e.Member.FirstName,
                        e.Member.LastName,
                    }
                })
                .ToArrayAsync();
        }

        [HttpGet("{assetId}/loan/current")]
        public async Task<ActionResult<object>> GetCurrentAssetLoan(int assetId)
        {
            return await context.AssetLoan
                .Include(e => e.Member)
                .Where(e => e.AssetId == assetId)
                .Where(e => e.DateReturned == null)
                .Select(e => new
                {
                    e.Id,
                    e.AssetId,
                    e.MemberId,
                    e.DateBorrowed,
                    e.DateReturned,
                    Member = new
                    {
                        e.Member.Id,
                        e.Member.FirstName,
                        e.Member.LastName,
                    }
                })
                .FirstOrDefaultAsync();
        }

        [HttpPost("{assetId}/loan/return")]
        public async Task<ActionResult<object>> ReturnCurrentAssetLoan(int assetId)
        {
            var currentLoan = await context.AssetLoan
                .Include(e => e.Member)
                .FirstOrDefaultAsync(e => e.AssetId == assetId && e.DateReturned == null);

            if (currentLoan != null)
            {
                currentLoan.DateReturned = DateTime.UtcNow;
                await context.SaveChangesAsync();
            }
            else
            {
                currentLoan = context.AssetLoan
                    .Include(e => e.Member)
                    .Where(e => e.AssetId == assetId)
                    .OrderByDescending(e => e.DateReturned)
                    .FirstOrDefault();
            }

            if (currentLoan == null)
            {
                return null;
            }

            return new
            {
                currentLoan.Id,
                currentLoan.AssetId,
                currentLoan.MemberId,
                currentLoan.DateBorrowed,
                currentLoan.DateReturned,
                Member = new
                {
                    currentLoan.Member.Id,
                    currentLoan.Member.FirstName,
                    currentLoan.Member.LastName,
                }
            };
        }

        [HttpPost("{assetId}/loan/to/member/{memberId}")]
        public async Task<ActionResult<object>> LoanAssetToMember(int assetId, int memberId)
        {
            if (!await context.Asset.AnyAsync(e => e.Id == assetId)) return NotFound();
            if (!await context.Member.AnyAsync(e => e.Id == memberId)) return NotFound();

            var currentLoan = await context.AssetLoan.FirstOrDefaultAsync(e => e.AssetId == assetId && e.DateReturned == null);
            if (currentLoan != null)
            {
                return BadRequest($"Asset is already on loan to member with Id {currentLoan.MemberId}");
            }

            var loanEntry = await context.AssetLoan.AddAsync(new AssetLoan
            {
                AssetId = assetId,
                MemberId = memberId,
                DateBorrowed = DateTime.UtcNow,
            });
            await context.SaveChangesAsync();

            return await context.AssetLoan
                .Include(e => e.Member)
                .Select(e => new
                {
                    e.Id,
                    e.AssetId,
                    e.MemberId,
                    e.DateBorrowed,
                    e.DateReturned,
                    Member = new
                    {
                        e.Member.Id,
                        e.Member.FirstName,
                        e.Member.LastName,
                    }
                })
                .FirstOrDefaultAsync(e => e.Id == loanEntry.Entity.Id);
        }
    }
}
