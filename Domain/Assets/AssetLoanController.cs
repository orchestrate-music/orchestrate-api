using System.Threading.Tasks;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace OrchestrateApi.Domain.Assets
{
    [Authorize(Policy = StandardAccessRequirement.PolicyName)]
    [ApiController]
    [Route("asset_loan")]
    public class AssetLoanController : ControllerBase
    {
        private readonly StandardEntityQueries<AssetLoan> queries;

        public AssetLoanController(OrchestrateContext context)
        {
            this.queries = new StandardEntityQueries<AssetLoan>(this, context);
        }

        [HttpPatch("{id}")]
        public Task<IActionResult> PatchAssetLoan(int id, JsonPatchDocument<AssetLoan> patchDocument)
        {
            return queries.Patch(id, patchDocument);
        }

        [HttpDelete("{id}")]
        public Task DeleteAssetLoan(int id)
        {
            return queries.Delete(id);
        }
    }
}