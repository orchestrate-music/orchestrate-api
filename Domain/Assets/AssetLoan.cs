using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OrchestrateApi.Domain.Members;

namespace OrchestrateApi.Domain.Assets
{
    public class AssetLoan : IEntity
    {
        [Key]
        public int Id { get; set; }

        public int MemberId { get; set; }

        public int AssetId { get; set; }

        public DateTime? DateBorrowed { get; set; }

        public DateTime? DateReturned { get; set; }

        [ForeignKey(nameof(AssetId))]
        public virtual Asset Asset { get; set; }

        [ForeignKey(nameof(MemberId))]
        public virtual Member Member { get; set; }
    }
}
