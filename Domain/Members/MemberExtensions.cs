using System;
using System.Linq;

namespace OrchestrateApi.Domain.Members
{
    public static class MemberExtensions
    {
        public static IQueryable<object> SelectMemberData(this IQueryable<Member> queryable)
        {
            return queryable.Select(e => ToMemberData(e));
        }

        public static object ToMemberData(this Member e)
        {
            return new
            {
                e.Id,
                e.FirstName,
                e.LastName,
                e.PhoneNo,
                e.Email,
                e.Address,
                e.FeeClass,
                e.MembershipClass,
                e.DateJoined,
                e.DateLeft,
                e.Notes,
                Ensemble = e.EnsembleMembership
                    .Where(e => e.DateLeft == null || e.DateLeft > DateTime.UtcNow)
                    .Select(e => new
                    {
                        e.Ensemble.Id,
                        e.Ensemble.Name,
                        e.Ensemble.IsHidden,
                    }),
                Instruments = e.MemberInstrument.Select(e => new
                {
                    e.Id,
                    e.MemberId,
                    e.InstrumentName,
                })
            };
        }

    }

}