using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using Microsoft.AspNetCore.JsonPatch;
using OrchestrateApi.Common;
using OrchestrateApi.Domain.Ensembles;
using Microsoft.AspNetCore.Authorization;
using OrchestrateApi.Security;

namespace OrchestrateApi.Domain.Members
{
    [Authorize(Roles = "Administrator,Editor")]
    [ApiController]
    [Route("member")]
    public class MemberController : ControllerBase
    {
        private readonly OrchestrateContext context;
        private readonly StandardEntityQueries<Member> queries;
        private readonly UserService userService;

        public MemberController(OrchestrateContext context, UserService userService)
        {
            this.context = context;
            this.queries = new StandardEntityQueries<Member>(this, context);
            this.userService = userService;
        }

        [HttpGet]
        public Task<IEnumerable<object>> GetMembers()
        {
            return GetMembers(false);
        }

        [HttpGet("current")]
        public Task<IEnumerable<object>> GetCurrentMembers()
        {
            return GetMembers(true);
        }

        private async Task<IEnumerable<object>> GetMembers(bool isActiveOnly)
        {
            return await context.Member
                .Include(e => e.MemberInstrument)
                .Include(e => e.EnsembleMembership)
                    .ThenInclude(e => e.Ensemble)
                .Where(e => !isActiveOnly || (e.DateLeft == null || e.DateLeft > DateTime.UtcNow))
                .SelectMemberData()
                .ToArrayAsync();
        }

        [HttpGet("{id}")]
        public Task<ActionResult<Member>> GetMember(int id)
        {
            return queries.Get(id);
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> PutMember(int id, JsonPatchDocument<Member> memberPatch)
        {
            var member = await context.Member
                .Include(i => i.User)
                .FirstOrDefaultAsync(i => i.Id == id);
            if (member != null)
            {
                memberPatch.ApplyTo(member);

                var memberChanges = context.ChangeTracker.Entries<Member>()
                    .FirstOrDefault(i => i.Entity == member);
                if (memberChanges != null && memberChanges.State == EntityState.Modified)
                {
                    var emailProperty = memberChanges.Property(i => i.Email);
                    if (member.User != null && emailProperty.IsModified)
                    {
                        await this.userService.UpdateUserEmail(member.User, member.Email);
                    }

                    var dateLeftProperty = memberChanges.Property(i => i.DateLeft);
                    if (member.DateLeft.HasValue && dateLeftProperty.IsModified)
                    {
                        var matchingMemberships = await context.EnsembleMembership
                            .Where(i => i.MemberId == id)
                            .Where(i => i.DateLeft == null || i.DateLeft == dateLeftProperty.OriginalValue)
                            .ToListAsync();
                        foreach (var membership in matchingMemberships)
                        {
                            membership.DateLeft = member.DateLeft;
                        }
                    }
                }

                await context.SaveChangesAsync();
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<ActionResult<Member>> PostMember(MemberDTO member)
        {
            await context.Member.AddAsync(member);
            var ensembleMembership = member.Ensembles.Select(ensembleId => new EnsembleMembership
            {
                Member = member,
                EnsembleId = ensembleId,
                DateJoined = DateTime.UtcNow,
            });
            await context.EnsembleMembership.AddRangeAsync(ensembleMembership);
            var memberInstruments = member.Instruments.Select(name => new MemberInstrument
            {
                Member = member,
                InstrumentName = name,
            });
            await context.MemberInstrument.AddRangeAsync(memberInstruments);
            await context.SaveChangesAsync();

            // Fetch so it is populated in .ToMemberData()
            await context.Ensemble.Where(e => member.Ensembles.Contains(e.Id)).ToArrayAsync();

            return CreatedAtAction(nameof(GetMember), new { id = member.Id }, member.ToMemberData());
        }

        [HttpDelete("{id}")]
        public async Task DeleteMember(int id)
        {
            var member = await context.Member.FindAsync(id);
            if (member == null)
            {
                return;
            }

            if (member.UserId != null)
            {
                await this.userService.DeleteUser(member.UserId);
            }

            context.Member.Remove(member);
            await context.SaveChangesAsync();
        }

        [HttpGet("{memberId}/ensemble_membership")]
        public async Task<IEnumerable<object>> GetMembership(int memberId)
        {
            return await context.EnsembleMembership
                .Include(e => e.Ensemble)
                .Where(e => e.MemberId == memberId)
                .Select(e => new
                {
                    e.Id,
                    e.MemberId,
                    e.EnsembleId,
                    e.DateJoined,
                    e.DateLeft,
                    Ensemble = new
                    {
                        e.Ensemble.Id,
                        e.Ensemble.Name,
                        e.Ensemble.IsHidden,
                    }
                })
                .ToArrayAsync();
        }

        [HttpPost("{memberId}/member_instrument")]
        public async Task<IEnumerable<object>> AddInstrument(int memberId, MemberInstrument instrument)
        {
            await context.MemberInstrument.AddAsync(new MemberInstrument
            {
                MemberId = memberId,
                InstrumentName = instrument.InstrumentName
            });
            await context.SaveChangesAsync();

            return await context.MemberInstrument
                .Where(e => e.MemberId == memberId)
                .Select(e => new
                {
                    e.Id,
                    e.MemberId,
                    e.InstrumentName,
                })
                .ToArrayAsync();
        }

        [HttpPost("{memberId}/invite")]
        public async Task InviteMember(int memberId)
        {
            await this.userService.InviteMember(memberId);
        }

        public class MemberDTO : Member
        {
            public int[] Ensembles { get; set; }

            public string[] Instruments { get; set; }
        }
    }
}
