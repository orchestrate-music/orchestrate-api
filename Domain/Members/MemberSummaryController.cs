using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using Microsoft.AspNetCore.Authorization;
using OrchestrateApi.Security;

namespace OrchestrateApi.Domain.Members
{
    [Authorize(Policy = StandardAccessRequirement.PolicyName)]
    [ApiController]
    [Route("member")]
    public class MemberSummaryController : ControllerBase
    {
        private readonly OrchestrateContext context;

        public MemberSummaryController(OrchestrateContext context)
        {
            this.context = context;
        }

        [HttpGet("{memberId}/concert")]
        public async Task<IEnumerable<object>> GetMemberConcerts(int memberId)
        {
            return await context.PerformanceMember
                .Where(e => e.MemberId == memberId)
                .Select(e => e.Performance.Concert)
                .Distinct()
                .Include(e => e.Performance)
                    .ThenInclude(e => e.PerformanceMembers)
                .Include(e => e.Performance)
                    .ThenInclude(e => e.Ensemble)
                .Select(e => new
                {
                    e.Id,
                    e.Date,
                    e.Location,
                    e.Notes,
                    e.Occasion,
                    Performance = e.Performance
                        .Where(e => e.PerformanceMembers.Select(e => e.MemberId).Contains(memberId))
                        .Select(e => new
                        {
                            e.Id,
                            e.EnsembleId,
                            e.ConcertId,
                            e.CountsAsSeperate,
                            Ensemble = new
                            {
                                e.Ensemble.Id,
                                e.Ensemble.Name,
                                e.Ensemble.IsHidden,
                            },
                        }),
                })
                .ToArrayAsync();
        }

        [HttpGet("summary")]
        public Task<IEnumerable<object>> GetMemberSummary()
        {
            return GetMemberSummary(false);
        }

        [HttpGet("current/summary")]
        public Task<IEnumerable<object>> GetCurrentMemberSummary()
        {
            return GetMemberSummary(true);
        }

        private async Task<IEnumerable<object>> GetMemberSummary(bool isActiveOnly)
        {
            return await context.Member
                .Include(e => e.MemberInstrument)
                .Include(e => e.EnsembleMembership)
                    .ThenInclude(e => e.Ensemble)
                .Include(e => e.PerformanceMember)
                .Where(e => !isActiveOnly || (e.DateLeft == null || e.DateLeft > DateTime.UtcNow))
                .Select(e => new
                {
                    e.Id,
                    e.FirstName,
                    e.LastName,
                    PerformanceCount = e.PerformanceMember.Count,
                    Ensemble = e.EnsembleMembership
                        .Where(e => e.DateLeft == null || e.DateLeft > DateTime.UtcNow)
                        .Select(e => new
                        {
                            e.Ensemble.Id,
                            e.Ensemble.Name,
                            e.Ensemble.IsHidden,
                        }),
                    Instruments = e.MemberInstrument.Select(e => new
                    {
                        e.Id,
                        e.MemberId,
                        e.InstrumentName,
                    })
                })
                .ToArrayAsync();
        }
    }
}
