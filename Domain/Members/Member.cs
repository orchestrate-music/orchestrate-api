using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OrchestrateApi.Domain.Assets;
using OrchestrateApi.Domain.Concerts;
using OrchestrateApi.Domain.Ensembles;
using OrchestrateApi.Security;

namespace OrchestrateApi.Domain.Members
{
    public class Member : IEntity
    {
        public Member()
        {
            AssetLoan = new HashSet<AssetLoan>();
            EnsembleMembership = new HashSet<EnsembleMembership>();
            MemberInstrument = new HashSet<MemberInstrument>();
            PerformanceMember = new HashSet<PerformanceMember>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [MinLength(12)]
        [MaxLength(12)]
        public string PhoneNo { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string FeeClass { get; set; }

        public string MembershipClass { get; set; }

        // TODO Convert DateJoined to computed property
        public DateTime? DateJoined { get; set; }

        // TODO Convert DateLeft to computed property
        public DateTime? DateLeft { get; set; }

        public string Notes { get; set; }

        public string UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public virtual OrchestrateUser User { get; set; }

        public virtual ICollection<AssetLoan> AssetLoan { get; set; }

        public virtual ICollection<EnsembleMembership> EnsembleMembership { get; set; }

        public virtual ICollection<MemberInstrument> MemberInstrument { get; set; }

        public virtual ICollection<PerformanceMember> PerformanceMember { get; set; }
    }
}
