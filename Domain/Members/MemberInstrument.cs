using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrchestrateApi.Domain.Members
{
    public class MemberInstrument
    {
        [Key]
        public int Id { get; set; }

        public int MemberId { get; set; }

        [Required]
        public string InstrumentName { get; set; }

        [ForeignKey(nameof(MemberId))]
        public virtual Member Member { get; set; }
    }
}
