using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using OrchestrateApi.Domain.Concerts;

namespace OrchestrateApi.Domain.Ensembles
{
    public class Ensemble : IEntity
    {
        public Ensemble()
        {
            EnsembleMembership = new HashSet<EnsembleMembership>();
            EnsembleRole = new HashSet<EnsembleRole>();
            Performance = new HashSet<Performance>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }


        public bool IsHidden { get; set; }

        public virtual ICollection<EnsembleMembership> EnsembleMembership { get; set; }
        public virtual ICollection<EnsembleRole> EnsembleRole { get; set; }
        public virtual ICollection<Performance> Performance { get; set; }
    }
}
