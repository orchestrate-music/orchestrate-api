using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using Microsoft.AspNetCore.JsonPatch;
using OrchestrateApi.Common;
using Microsoft.AspNetCore.Authorization;
using OrchestrateApi.Security;

namespace OrchestrateApi.Domain.Ensembles
{
    [Authorize(Policy = StandardAccessRequirement.PolicyName)]
    [ApiController]
    [Route("ensemble")]
    public class EnsembleController : ControllerBase
    {
        private readonly OrchestrateContext context;
        private readonly StandardEntityQueries<Ensemble> queries;

        public EnsembleController(OrchestrateContext context)
        {
            this.context = context;
            this.queries = new StandardEntityQueries<Ensemble>(this, context);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<object>>> GetEnsembles()
        {
            return await context.Ensemble
                .Include(e => e.Performance)
                .Include(e => e.EnsembleMembership)
                    .ThenInclude(e => e.Member)
                .Select(e => new
                {
                    e.Id,
                    e.Name,
                    e.IsHidden,
                    PerformanceCount = e.Performance.Count,
                    Member = e.EnsembleMembership
                        .Where(e => e.DateLeft == null || e.DateLeft > DateTime.UtcNow)
                        .Select(e => new
                        {
                            e.Member.Id,
                            e.Member.FirstName,
                            e.Member.LastName,
                        }),
                })
                .ToArrayAsync();
        }

        [HttpGet("{id}")]
        public Task<ActionResult<Ensemble>> GetEnsemble(int id)
        {
            return queries.Get(id);
        }

        [HttpPatch("{id}")]
        public Task<IActionResult> PutEnsemble(int id, JsonPatchDocument<Ensemble> ensemblePatch)
        {
            return queries.Patch(id, ensemblePatch);
        }

        [HttpPost]
        public Task<ActionResult<Ensemble>> PostEnsemble(Ensemble ensemble)
        {
            return queries.Post(ensemble, nameof(GetEnsemble));
        }

        [HttpDelete("{id}")]
        public Task DeleteEnsemble(int id)
        {
            return queries.Delete(id);
        }

        [HttpGet]
        [Route("{ensembleId:int}/ensemble_membership/current")]
        public async Task<ActionResult<IEnumerable<object>>> GetMembers(int ensembleId)
        {
            return await context.EnsembleMembership
                .Include(e => e.Member)
                .Where(e => e.EnsembleId == ensembleId)
                .Where(e => e.DateLeft == null || e.DateLeft > DateTime.UtcNow)
                .Select(e => new
                {
                    e.Member.Id,
                    e.Member.FirstName,
                    e.Member.LastName,
                })
                .ToArrayAsync();
        }

        [HttpPost]
        [Route("{ensembleId}/ensemble_membership/{memberId}/join")]
        public async Task<object> JoinEnsemble(int ensembleId, int memberId)
        {
            var member = await context.Member.FindAsync(memberId);
            if (member == null)
            {
                return NotFound();
            }

            if (member.DateLeft.HasValue)
            {
                member.DateLeft = null;
            }

            var membership = new EnsembleMembership
            {
                EnsembleId = ensembleId,
                MemberId = memberId,
                DateJoined = DateTime.UtcNow,
            };
            context.EnsembleMembership.Add(membership);

            await context.SaveChangesAsync();

            return new
            {
                membership.Id,
                membership.DateJoined,
                membership.DateLeft,
                membership.EnsembleId,
                membership.MemberId,
            };
        }

        [HttpPost]
        [Route("{ensembleId}/ensemble_membership/{memberId}/leave")]
        public async Task<IActionResult> LeaveEnsemble(int ensembleId, int memberId)
        {
            var member = await context.Member.FindAsync(memberId);
            if (member == null)
            {
                return NotFound();
            }

            var membership = await context.EnsembleMembership
                .Where(e => e.EnsembleId == ensembleId && e.MemberId == memberId)
                .FirstOrDefaultAsync();

            membership.DateLeft = DateTime.UtcNow;

            var hasActiveMemberships = await context.EnsembleMembership
                .Where(i => i.MemberId == memberId)
                .AnyAsync(i => i.DateLeft == null || i.DateLeft > DateTime.UtcNow);
            if (!hasActiveMemberships)
            {
                member.DateLeft = membership.DateLeft;
            }

            await context.SaveChangesAsync();
            return NoContent();
        }
    }
}
