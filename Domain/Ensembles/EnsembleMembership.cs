using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OrchestrateApi.Domain.Members;

namespace OrchestrateApi.Domain.Ensembles
{
    public class EnsembleMembership : IEntity
    {
        [Key]
        public int Id { get; set; }

        public int MemberId { get; set; }

        public int EnsembleId { get; set; }

        public DateTime? DateJoined { get; set; }

        public DateTime? DateLeft { get; set; }

        [ForeignKey(nameof(EnsembleId))]
        public virtual Ensemble Ensemble { get; set; }

        [ForeignKey(nameof(MemberId))]
        public virtual Member Member { get; set; }
    }
}
