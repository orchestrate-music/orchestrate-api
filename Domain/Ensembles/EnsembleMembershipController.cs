using System.Threading.Tasks;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace OrchestrateApi.Domain.Ensembles
{
    [Authorize(Policy = StandardAccessRequirement.PolicyName)]
    [ApiController]
    [Route("ensemble_membership")]
    public class EnsembleMembershipController : ControllerBase
    {
        private readonly StandardEntityQueries<EnsembleMembership> queries;

        public EnsembleMembershipController(OrchestrateContext context)
        {
            this.queries = new StandardEntityQueries<EnsembleMembership>(this, context);
        }

        [HttpPatch("{id}")]
        public Task<IActionResult> PatchEnsembleMembership(int id, JsonPatchDocument<EnsembleMembership> membershipPatch)
        {
            return queries.Patch(id, membershipPatch);
        }
    }
}