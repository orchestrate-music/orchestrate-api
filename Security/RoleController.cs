using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace OrchestrateApi.Security
{
    [Authorize(Roles = "Administrator")]
    [ApiController]
    [Route("role")]
    public class RoleController : ControllerBase
    {
        private readonly OrchestrateContext context;

        public RoleController(OrchestrateContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<object>> GetRoles()
        {
            return await context.Roles.Select(e => new { e.Id, Role = e.Name }).ToListAsync();
        }
    }
}