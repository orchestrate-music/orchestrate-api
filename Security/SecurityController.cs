using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using OrchestrateApi.Email;
using OrchestrateApi.Domain.Members;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace OrchestrateApi.Security
{
    [ApiController]
    [Route("security")]
    public class SecurityController : ControllerBase
    {
        private readonly OrchestrateContext context;
        private readonly SignInManager<OrchestrateUser> signInManager;
        private readonly UserManager<OrchestrateUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserService userService;
        private readonly EmailService emailService;
        private readonly AppConfiguration configuration;
        private readonly IHttpClientFactory clientFactory;

        public SecurityController(
            OrchestrateContext context,
            SignInManager<OrchestrateUser> signInManager,
            UserManager<OrchestrateUser> userManager,
            RoleManager<IdentityRole> roleManager,
            UserService userService,
            EmailService emailService,
            AppConfiguration configuration,
            IHttpClientFactory clientFactory)
        {
            this.context = context;
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.userService = userService;
            this.emailService = emailService;
            this.configuration = configuration;
            this.clientFactory = clientFactory;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginModel model)
        {
            var user = await userManager.FindByNameAsync(model.UserName);
            if (user == null)
            {
                var result = await ValidateUserFromOldApiAsync(model);
                if (!result.Item1)
                {
                    return Unauthorized();
                }

                user = await CreateUserFromLoginModelAsync(model, result.Item2);
            }
            else if (!await userManager.CheckPasswordAsync(user, model.Password))
            {
                return Unauthorized();
            }

            var token = await this.userService.GenerateBearerTokenAsync(user);
            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = token.ValidTo,
                user = new
                {
                    username = model.UserName,
                    roles = token.Claims
                        .Where(i => i.Type == ClaimTypes.Role)
                        .Select(r => new { role = r.Value }),
                }
            });
        }

        private async Task<Tuple<bool, string[]>> ValidateUserFromOldApiAsync(LoginModel model)
        {
            const string csrfToken = "38f1d75fbf374f3b52f17072b43d550f9f0ab44aa8bab8bbe225f9e7ddb7149d4844aa66fcc7e451f7efc22d729dba46f9f5435163b89ad8ca8d650cbcf2ddf6";
            var request = new HttpRequestMessage(HttpMethod.Post, "https://api.lacb.org.au/security/login");
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("Cookie", $"csrfToken={csrfToken}");
            request.Headers.Add("X-CSRF-Token", csrfToken);
            var content = JsonConvert.SerializeObject(new
            {
                username = model.UserName,
                password = model.Password,
            });
            request.Content = new StringContent(content, Encoding.UTF8, "application/json");

            var client = this.clientFactory.CreateClient();
            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                return Tuple.Create(false, Array.Empty<string>());
            };

            var jsonResponse = JsonConvert.DeserializeObject<JObject>(await response.Content.ReadAsStringAsync());
            var roles = jsonResponse["user"]["roles"].Select(i => (string)i["role"]).ToArray();
            return Tuple.Create(true, roles);
        }

        private async Task<OrchestrateUser> CreateUserFromLoginModelAsync(LoginModel model, string[] roles)
        {
            var member = await this.context.Member.FirstOrDefaultAsync(i => i.Email == model.UserName);
            var user = new OrchestrateUser
            {
                SecurityStamp = Guid.NewGuid().ToString(),
                Email = member?.Email,
                UserName = model.UserName,
            };

            if (member != null)
            {
                member.User = user;
            }

            var result = await this.userManager.CreateAsync(member.User, model.Password);
            if (!result.Succeeded)
            {
                throw new InvalidOperationException("Unable to create user");
            }

            if (!await this.userManager.IsEmailConfirmedAsync(user))
            {
                var token = await this.userManager.GenerateEmailConfirmationTokenAsync(user);
                await this.userManager.ConfirmEmailAsync(user, token);
            }

            var roleMapping = new Dictionary<string, string>
            {
                ["admin"] = "Administrator",
                ["editor"] = "Editor",
                ["viewer"] = "Viewer",
            };
            var mappedRoles = roles.Select(i => roleMapping[i]);
            var roleResult = await this.userManager.AddToRolesAsync(user, mappedRoles);
            if (!roleResult.Succeeded)
            {
                throw new InvalidOperationException("Failed to assign roles to new user");
            }

            return user;
        }

        [HttpPost("logout")]
        public void Logout()
        {
            return;
        }

        [HttpGet("user")]
        public IActionResult GetUser()
        {
            if (User == null)
            {
                return Unauthorized();
            }

            var usernameClaim = User.Claims.FirstOrDefault(e => e.Type == ClaimTypes.Name);
            if (usernameClaim == null)
            {
                return Unauthorized();
            }

            var roleClaims = User.Claims.Where(e => e.Type == ClaimTypes.Role);
            return Ok(new
            {
                username = usernameClaim.Value,
                roles = roleClaims.Select(r => new { role = r.Value }),
            });
        }

        [HttpPost("reset_password")]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel model)
        {
            var user = await this.userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return BadRequest();
            }

            var result = await this.userManager.ResetPasswordAsync(user, model.Token, model.Password);
            if (!result.Succeeded)
            {
                var messages = result.Errors.Select(error => $"{error.Code} - {error.Description}");
                return BadRequest(string.Join(", ", messages));
            }

            if (!await this.userManager.IsEmailConfirmedAsync(user))
            {
                var token = await this.userManager.GenerateEmailConfirmationTokenAsync(user);
                await this.userManager.ConfirmEmailAsync(user, token);
            }

            return Ok();
        }

        [HttpPost("request_password_reset")]
        public async Task RequestResetPassword(RequestResetPasswordModel model)
        {
            await this.userService.RequestResetPassword(model.Email);
        }

        public class LoginModel
        {
            public string UserName { get; set; }

            public string Password { get; set; }
        }

        public class ResetPasswordModel
        {
            public string Email { get; set; }

            public string Token { get; set; }

            public string Password { get; set; }
        }

        public class RequestResetPasswordModel
        {
            public string Email { get; set; }
        }
    }
}